import smtplib
#from email.MIMEMultipart import MIMEMultipart
#from email.MIMEText import MIMEText


# example alerter class to send an email. can ping chat bots or
# any number of other things
class Alerter(object):
    def email(self, msg_json):
        # fill in from, to, and smtplib.SMTP as appropriate
        msg = MIMEMultipart()
        msg['Subject'] = "Service {} Alert!".format(msg_json["service"])
        msg['From'] = ""  # fill
        msg["To"] = ""  # fill
        text = """
            The service {} has encountered the event {} enough times to trigger an alert!
        """.format(msg_json["service"], msg_json["event_name"])
        msg.attach(MIMEText(text))

        sender = smtplib.SMTP("") # fill
        sender.starttls()
        sender.sendmail(msg["From"], msg["To"], msg.as_string())

    def alert(self, msg_json):
        # in a real setting, could send an email, ping a discord/slack bot,
        # or any other alert actions that would like to be taken
        # self.email(msg_json)
        pass
