import logging
import requests


EVENT_ADDRESS = "http://localhost:8080/message"


# Pings the above address with a message
def event(service, event_name, msg):
    post_data = {
        "service": service,
        "event_name": event_name,
        "msg": msg
    }
    r = requests.post(EVENT_ADDRESS, json=post_data)
    logging.info(r.content)
    print(r.content)
    if r.status_code != 200:
        logging.error("Logging dev alert message failed!")
        logging.error(post_data)
