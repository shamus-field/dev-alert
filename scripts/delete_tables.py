import psycopg2


# Cleans up all tables needed for program running
def delete_tables(cursor):
    cursor.execute("drop table if exists event;")
    cursor.execute("drop table if exists event_group;")
    cursor.execute("drop table if exists alert;")
    cursor.execute('drop extension if exists "uuid-ossp";')

def main():
    conn = psycopg2.connect(dbname="devalert", user="postgres", password="postgres", host="localhost")
    cursor = conn.cursor()

    delete_tables(cursor)

    conn.commit()


if __name__ == "__main__":
    main()

