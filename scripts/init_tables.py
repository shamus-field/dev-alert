import json
import os
import psycopg2


# Inits all tables required for main webserver running
def setup_tables(cursor, event_configs):
    cursor.execute('create extension if not exists "uuid-ossp";')
    cursor.execute("""
        create table alert (
            id uuid primary key default uuid_generate_v4(),
            create_time timestamp default now(),
            ack_time timestamp default null
        );
        create table event_group (
            id uuid primary key default uuid_generate_v4(),
            alert_id uuid references alert (id),
            service text not null,
            event_name text not null,
            reset_minutes int not null,
            create_time timestamp default now()
        );
        create table event (
            message text not null,
            group_id uuid references event_group (id),
            create_time timestamp default now()
        );
    """)

def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    config_path = os.path.join(dir_path, "..", "event_config.json")
    if not os.path.exists(config_path):
        print("Could not find event config file, please create.")
        quit()

    with open(config_path) as f:
        event_configs = json.loads(f.read())

    conn = psycopg2.connect(dbname="devalert", user="postgres", password="postgres", host="localhost")
    cursor = conn.cursor()

    setup_tables(cursor, event_configs)

    conn.commit()


if __name__ == "__main__":
    main()




