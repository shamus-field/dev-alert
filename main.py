import json
import logging
import os

import cerberus
import cherrypy
import mako
from mako.lookup import TemplateLookup

from message_handler import MessageHandler


class DevAlert(object):
    def __init__(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        config_path = os.path.join(dir_path, "event_config.json")

        if not os.path.exists(config_path):
            print("Could not find event config file, please create.")
            quit()

        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(logging.StreamHandler())
        self.logger.info("Loading config file.")
        with open(config_path) as f:
            self.event_configs = json.loads(f.read())

        self.lookup = TemplateLookup(directories=["mako"])

    def render(self, template, **kwargs):
        try:
            return self.lookup.get_template(template).render(**kwargs)
        except:
            return mako.exceptions.html_error_template().render()

    @cherrypy.expose
    def index(self):
        return self.render("index.html", navbar=[])

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def message(self):
        json_msg = cherrypy.request.json
        self.logger.info("Received event message")
        self.logger.info(json_msg)

        schema = {"service": {"type": "string", "required": True},
                  "event_name": {"type": "string", "required": True},
                  "msg": {"type": "string", "required": True}}
        v = cerberus.Validator(schema)
        if not v.validate(json_msg):
            response_str = "Received message does not adhere to schema: {}"
            return json.dumps({"response": response_str.format(schema)})

        if json_msg["service"] not in self.event_configs.keys():
            response_str = "Service {} not found in devalert config!"
            return json.dumps({"response": response_str.format(json_msg["service"])})

        if json_msg["event_name"] not in self.event_configs[json_msg["service"]].keys():
            response_str = "Event name {} not found in devalert config!"
            return json.dumps({"response": response_str.format(json_msg["event_name"])})

        MessageHandler(self.event_configs).insert_message(json_msg)
        return json.dumps({"response": "success"})

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def ack_alert(self):
        json_msg = cherrypy.request.json

        schema = {"service": {"type": "string", "required": True},
                  "event_name": {"type": "string", "required": True}}
        v = cerberus.Validator(schema)
        if not v.validate(json_msg):
            response_str = "Received acknowledge does not adhere to schema: {}"
            return json.dumps({"response": response_str.format(schema)})

        if json_msg["service"] not in self.event_configs.keys():
            response_str = "Service {} not found in devalert config!"
            return json.dumps({"response": response_str.format(json_msg["service"])})

        if json_msg["event_name"] not in self.event_configs[json_msg["service"]].keys():
            response_str = "Event name {} not found in devalert config!"
            return json.dumps({"response": response_str.format(json_msg["event_name"])})

        MessageHandler(self.event_configs).acknowledge_alert(json_msg)
        return json.dumps({"response": "success"})

def main():
    settings = {
        "global": {

        },
        "/": {
            "tools.staticdir.root": "C:\\new_farm\\"
        },
        "/static": {
            "tools.staticdir.on": True,
            "tools.staticdir.dir": "static"
        }
    }

    cherrypy.quickstart(DevAlert(), "/", config=settings)


if __name__ == "__main__":
    main()
