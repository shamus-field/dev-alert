import datetime
import logging
import psycopg2
import psycopg2.extras


from alerter import Alerter


DB_CONN_ARGS = {
    "dbname": "devalert",
    "user": "postgres",
    "password": "postgres",
    "host": "localhost"
}


def set_obj_vals(obj, row, cursor):
    if row:
        i = 0
        for col in row:
            setattr(obj, cursor.description[i].name, col)
            i += 1


class MessageHandler(object):
    def __init__(self, event_configs, db_args=None):
        self.logger = logging.getLogger()
        self.event_configs = event_configs
        if not db_args:
            self.db_args = DB_CONN_ARGS
        else:
            self.db_args = db_args

    def insert_message(self, msg):
        """Inserts a message into the database, and handles assorted record
        creation of event groups and alerts where needed. Note: I decided that
        an event group's alert being already acknowledged should re-prompt an
        alert because if a dev acknowledged an alert further messages for the
        event would be swallowed until the reset minutes triggers on the group,
        which I feel is a worse scenario than someone getting one more email
        about the problem.

        General order of events:
        attempt to fetch previous event group
        if it exists:
            fetch the group alert
            if the reset minutes + create time > now
                OR there is an alert for the event group already acknowledged:
                    create new event group
            else:
                use old event group
        else:
            create a new event group

        insert the event record

        attempt to fetch the event group's alert
        if it does not exist or has not been acknowledged:
            create a new alert
        else:
            do nothing (don't want to spam alerts)
        """
        config_reset_minutes = self.event_configs[msg["service"]][msg["event_name"]]["reset_minutes"]
        config_count = self.event_configs[msg["service"]][msg["event_name"]]["count"]

        with psycopg2.connect(**self.db_args) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
                event_group_id = None

                cursor.execute("""
                    select *
                    from event_group
                    where service = %s
                    and event_name = %s
                    order by create_time desc
                    limit 1;
                """, (msg["service"], msg["event_name"]))

                event_group = cursor.fetchone()
                if event_group:
                    self.logger.info("Found corresponding group for event")
                    cursor.execute("""
                        select * from alert
                        where id = %s;
                    """, (event_group["alert_id"],))
                    alert = cursor.fetchone()

                    max_lifetime = event_group["create_time"] + datetime.timedelta(minutes=event_group["reset_minutes"])
                    if max_lifetime < datetime.datetime.now() or (alert and alert["ack_time"]):
                        self.logger.info("Event group was expired or had an acknowledged alert, creating new group.")
                        cursor.execute("""
                            insert into event_group values (
                                default,
                                null,
                                %s,
                                %s,
                                %s,
                                default
                            ) returning id;
                        """, (msg["service"], msg["event_name"], config_reset_minutes))
                        event_group_id = cursor.fetchone()["id"]
                    else:
                        self.logger.info("Event group is current, using found group.")
                        event_group_id = event_group["id"]
                else:
                    self.logger.info("No event group found, creating new group.")
                    cursor.execute("""
                        insert into event_group values (
                            default,
                            null,
                            %s,
                            %s,
                            %s,
                            default
                        ) returning id;
                    """, (msg["service"], msg["event_name"], config_reset_minutes))
                    event_group_id = cursor.fetchone()["id"]

                cursor.execute("""
                    insert into event values (
                        %s,
                        %s,
                        default
                    );
                """, (msg["msg"], event_group_id))

                cursor.execute("""
                    select count(*) from event
                    where group_id = %s
                """, (event_group_id,))
                count = cursor.fetchone()["count"]

                if count >= config_count:
                    self.logger.info("Current event count can trigger an alert.")
                    cursor.execute("""
                        select * from alert
                        where id = %s;
                    """, (event_group["alert_id"],))
                    alert = cursor.fetchone()

                    if not alert:
                        self.logger.info("No alert was found, creating new alert.")
                        cursor.execute("""
                            insert into alert values (
                                default,
                                default,
                                default
                            ) returning id;
                        """)
                        new_alert = cursor.fetchone()
                        cursor.execute("""
                            update event_group
                            set alert_id = %s
                            where id = %s
                        """, (new_alert["id"], event_group_id))

                        # left open to user what to do here (email/bot ping/etc)
                        Alerter().alert(msg)
                    else:
                        self.logger.info("Un-acknowledged alert already exists for event group.")

                conn.commit()

    def acknowledge_alert(self, msg):
        with psycopg2.connect(**self.db_args) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
                cursor.execute("""
                    select *
                    from event_group
                    where service = %s
                    and event_name = %s
                    order by create_time desc
                    limit 1;
                """, (msg["service"], msg["event_name"]))
                event_group = cursor.fetchall()

                # currently this will allow an acknowledge on something already
                # acknowledged, but I can't really think of a reason you shouldn't
                # be allowed to do that? maybe just for historical accuracies sake...
                # thinking about that some more
                # leaving as TODO: prevent (or not) acknowledging already acknowledged alert
                if event_group and event_group["alert_id"]:
                    cursor.execute("""
                        update alert
                        set ack_time = now()
                        where id = %s
                    """, (event_group["alert_id"],))

                conn.commit()

