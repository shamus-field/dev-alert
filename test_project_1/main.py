import sys

from scripts.event_script import event


# Example program to show usage of unhandled exception block
# and normal message sending

def unhandled_exception(exc_type, exc_value, exc_traceback):
    # skip if someone did a ctrl-c to kill the program on purpose
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    event("service1", "unhandled_exception", "Exception: {} {}".format(
        exc_type, exc_value))

    # this step is optional, depending on whether you want
    # to swallow or continue on with the unhandled exception
    # but generally speaking if you've got something
    # catastrophic you don't want to just swallow it
    sys.__excepthook__(exc_type, exc_value, exc_traceback)


def main():
    sys.excepthook = unhandled_exception

    try:
        do_something()
    except Exception:
        event("service1", "occasional_exception", "3rd party service was down!  000")
        #raise

    event("service1", "occasional_exception", "3rd party service was down!  111")
    event("service1", "occasional_exception", "3rd party service was down!  222")
    event("service1", "occasional_exception", "3rd party service was down!  333")
    event("service1", "occasional_exception", "3rd party service was down!  444")

    #unhandled = 1 / 0

if __name__ == "__main__":
    main()
