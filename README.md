## DevAlert

DevAlert is a small python webserver which receives messages from
other python services/programs. The intent is to use either an
unhandled exception block in a service, or just make a standalone
event call, to send the event messages over to the webserver.

Events have an associated count, which indicates when a dev
should be alerted that there is a problem. The intention there
is that for certain events, a dev might not want to know until
there's been a threshold reached. For example, an unreliable 3rd
party service may go down from time to time - the service may not
care unless a large number of calls have failed. Other events,
such as unhandled exceptions, should always alert a dev, as something
critically wrong has likely happened in that case.

Once an alert has been created, no more will be created (so as to
not spam a dev's inbox) until the previous one has been acknowledged
via the acknowledge endpoint.

### Install

DevAlert will require the following pip installations:
> pip install cherrypy requests psycopg2 mako cerberus

Postgres version 11 or higher. To create databases:

> createdb devalert

If test running is desired:

> createdb devalert_test

### Running
To run the DevAlert server, simply run python main.py. The
event_script.py file may require a change to the message url
if not running the DevAlert server on the same machines as
the messaging service.

### Config
An example config file has been provided, simply modify that
to the needed use case. Generally, the format is as follows:

    {
        "service name": {
            "event name": "count",
            "event name": "count"
        },
        "other service name": {
            "event name": "count",
            "event name': "count"
        }
    }

Service names and event names are strings, and event counts
are integers.

### Example

Included in test_project_1 is a small script which shows an
example of both an unhandled exception block as well as a
regular event call.

### Scripts

Included for easy setup and teardown of database tables are
init_tables.py and delete_tables.py. Event_script.py is
the script called by a service which requires monitoring.

### Additional changes and todos
* Include better config file with db connection params
* Use argparse for command line options
* Provide endpoint & mako template/frontend for service stats
* Allow for a time based component to events (ie x events in an hour trigger alert)
* Allow generic event messages to be stored in event table & extras like var values
* Automatically update db schema if config file is changed (or at least blow up...)
* Move event_script.py into an pip installable package for easier deployment & usage
