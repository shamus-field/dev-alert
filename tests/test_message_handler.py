import unittest

import psycopg2

from message_handler import MessageHandler
from scripts.init_tables import setup_tables
from scripts.delete_tables import delete_tables


TEST_EVENT_CONFIG = {
    "test1": {
        "test_event": 5
    },
    "test2": {
        "test_event2": 1
    }
}

TEST_DB_ARGS = {
    "dbname": "devalert_test",
    "user": "postgres",
    "password": "postgres",
    "host": "localhost"
}


class TestMessageHandler(unittest.TestCase):
    def setUp(self):
        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        # if an exception occurs in tested method then the teardown will not run,
        # thus delete these tables before each setup
        delete_tables(cursor)
        setup_tables(cursor, TEST_EVENT_CONFIG)
        conn.commit()

    def tearDown(self):
        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        delete_tables(cursor)
        conn.commit()

    def test_insert_message_no_alert(self):
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.insert_message({"service": "test1", "event_name": "test_event"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        result = cursor.execute("select * from event where service = 'test1' and event_name = 'test_event'").fetchall()
        conn.commit()
        assert(result[0][3] == 1)

    def test_insert_message_create_alert(self):
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        cursor.execute("select * from alert")
        results = cursor.fetchall()
        conn.commit()
        assert len(results) == 1
        assert results[0][3] is None

    def test_insert_message_alert_already_exists(self):
        # there shouldn't be another alert if one already exists
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        cursor.execute("select * from alert")
        results = cursor.fetchall()
        conn.commit()
        assert len(results) == 1
        assert results[0][3] is None

    def test_acknowledge_alert(self):
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})
        to_test.acknowledge_alert({"service": "test2", "event_name": "test_event2"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        cursor.execute("select * from alert")
        results = cursor.fetchall()
        conn.commit()
        assert len(results) == 1
        assert results[0][3] is not None

    def test_insert_message_last_alert_acked(self):
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})
        to_test.acknowledge_alert({"service": "test2", "event_name": "test_event2"})
        to_test.insert_message({"service": "test2", "event_name": "test_event2"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        cursor.execute("select * from alert order by create_time desc")
        results = cursor.fetchall()
        conn.commit()
        assert len(results) == 2
        assert results[0][3] is None

    def test_acknowledge_alert_no_alert(self):
        to_test = MessageHandler(TEST_EVENT_CONFIG, TEST_DB_ARGS)
        to_test.acknowledge_alert({"service": "test2", "event_name": "test_event2"})

        conn = psycopg2.connect(**TEST_DB_ARGS)
        cursor = conn.cursor()
        cursor.execute("select * from alert")
        results = cursor.fetchall()
        conn.commit()
        assert len(results) == 0

if __name__ == "__main__":
    unittest.main()
